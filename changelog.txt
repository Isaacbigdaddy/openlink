v1.4.1
- set minimum API level to 3 (Android 1.5)
- minor code improvements
- tried to improve Chinese translations (added traditional and simplified) in addition to regular zn-locale

v1.4.0
- improved icons
- removed entry from launcher, Activity can only be accessed via "open"-button in "Play Store"- oder "F-Droid"-app
- changed max. API level to 23 (Android 6)

v1.3.1
- updated source code repository URL (moved from Gitorious to GitLab)
- improved layout
- changed max. API level to 22 (Android 5.1)

v1.3
- support for Material design (Android 5.0 and above)
- improved URL parsing (thanks to R. Wong)

v1.2.1
- removed flicker when links are opened

v1.2
- added translations for the following languages: Arabic, Bulgarian, Catalan, Czech, Danish, Spanish, Finnish, French, Italian, Japanese, Dutch, Norwegian, Polish, Portuguese, Russian, Swedish, Thai, Chinese

v1.1
- brackets and punctuation characters enclosing URLs will be ignored (Thanks to DB for the idea.)

v1.0.2
- more UI improvements, friendlier info screen

v1.0.1
- minor UI improvements in info screen

v1.0
- initial release


